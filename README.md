**CatsOne drupal 8 module**

Drupal 8 CatsOne module will provide you a nice Drupal interface for easy management your CatsOne content.
This module collaborate with CatsOne API.

Module is still in progress.

CatsOne : https://www.catsone.com/
CATS makes organizing and searching through your data a breeze. Collect job applications, sync your email, and Boolean search inside resumes to find the best candidates in half the time.

Git clone : git clone git@bitbucket.org:dzoda/catsone.git

