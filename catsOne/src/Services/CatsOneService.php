<?php

namespace Drupal\catsOne\Services;

use Drupal;
use Drupal\Core\State\State;
use GuzzleHttp\Exception\RequestException;

/**
 * Class CatsOneService.
 */
class CatsOneService {

  public $catsOneApiPath;

  public $state;


  /**
   * CatsOneService constructor.
   */
  public function __construct(State $state) {
    $this->state = $state;
    $this->catsOneApiPath = 'https://api.catsone.com/v3/';
  }

  /**
   * Method to validate API key.
   */
  public function getCatsOneInfo($catsOneKey) {
    try {
      $response = \Drupal::httpClient()
        ->get($this->catsOneApiPath . 'site', ['headers' => ['Authorization' => 'Token ' . $catsOneKey]]);
      $data = (string) $response->getBody();
      if (!empty($data)) {
        $data = json_decode($data);
        $userId = $data->_embedded->users[0]->id;
        $username = $data->_embedded->users[0]->username;
        $fname = $data->_embedded->users[0]->first_name;
        $lname = $data->_embedded->users[0]->last_name;
        drupal_set_message('The API key is valid.', 'status');
        drupal_set_message('User ID : ' . $userId, 'status');
        drupal_set_message('Username : ' . $username, 'status');
        drupal_set_message('First name : ' . $fname, 'status');
        drupal_set_message('Last name : ' . $lname, 'status');
        $this->state->set('catsOneApiKey', $catsOneKey);
      }
    }
    catch (RequestException $e) {
      drupal_set_message('The API key is not valid.', 'warning');
      return;
    }
  }

  /**
   * Get all CatsOne Candidates.
   */
  public function getAllCatsOneCandidates() {
    $data = [];
    $path = $this->catsOneApiPath . 'candidates';
    $options = $this->catsOneGet();

    try {
      $response = \Drupal::httpClient()->get($path,$options);
      $data = (string) $response->getBody();
    }
    catch (RequestException $e) {
      drupal_set_message('Something went wrong ! Please try again.', 'warning');
    }

    return $data;
  }

  /**
   * Get all CatsOne Jobs.
   */
  public function getAllCatsOneJobs() {
    $data = [];
    $path = $this->catsOneApiPath . 'jobs';
    $options = $this->catsOneGet();

      try {
        $response = \Drupal::httpClient()->get($path,$options);
        $data = (string) $response->getBody();
      }
      catch (RequestException $e) {
        drupal_set_message('Something went wrong ! Please try again.', 'warning');
      }

    return $data;
  }

  /**
   * Get all CatsOne Jobs.
   */
  public function getAllCatsOneCompanies() {
    $data = [];
    $path = $this->catsOneApiPath . 'companies';
    $options = $this->catsOneGet();

    try {
      $response = \Drupal::httpClient()->get($path,$options);
      $data = (string) $response->getBody();
    }
    catch (RequestException $e) {
      drupal_set_message('Something went wrong ! Please try again.', 'warning');
    }

    return $data;
  }

  /**
   * Import CatsOne bundle.
   */
  public function importCatsOneBundle($bundle , $value) {

    $data = [
      'cats_one_type' => $bundle,
      'cats_one_data' => $value,
    ];
    $catsCandidateStatus = Drupal::entityTypeManager()
      ->getStorage('cats_one_entity')
      ->create($data);
    $catsCandidateStatus->save();
  }

  /**
   * Select all Candidates bundle.
   */
  public function selectAllBundle($bundle) {

    $query = \Drupal::entityQuery('cats_one_entity');
    $query->condition('cats_one_type', $bundle);
    $entity_ids = $query->execute();

    return $entity_ids;
  }

  /**
   * Load CatsOne bundle.
   */
  public function loadCatsOneBundle($ids) {
    $entity = \Drupal::entityTypeManager()->getStorage('cats_one_entity')->loadMultiple($ids);
    return $entity;
  }

  /**
   * CatsOne table.
   */
  public function  loadCatsOneBundleTable($bundle, $header) {
    $db = \Drupal::database();
    $query = $db->select('cats_one_entity','c');
    $query->fields('c', array('id','cats_one_data','cats_one_type'));
    $query->condition('cats_one_type', $bundle);
    // The actual action of sorting the rows is here.
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header);
    // Limit the rows to 20 for each page.
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(20);
    $result = $pager->execute();

    return $result;
  }

  /**
   * Load catsOne Candidates.
   */
  public function loadCatsOneCandidateById($id) {
    $entity = \Drupal::entityTypeManager()->getStorage('cats_one_entity')->load($id);
    return $entity;
  }

  /**
   * Delete catsOne bundle.
   */
  public function deleteCatsOneBundleById($id) {
    $db = \Drupal::database();
    $query = $db->delete('cats_one_entity');
    $query->condition('id', $id);
    $query->execute();
  }

  /**
   * Get candidates additional fields.
   */
  public function getAdditionalFields($bundle) {
    try {
      $options = $this->catsOneGet();
      $path = $this->catsOneApiPath . $bundle . '/custom_fields';
      $response = \Drupal::httpClient()->get($path, $options);
      $data = (string) $response->getBody();
      if ($data) {
        $data = json_decode($data);
        $custom_fields = (!empty($data->_embedded->custom_fields)) ? $data->_embedded->custom_fields : NULL;
        return $custom_fields;
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }
  }

  /**
   * Delete catsOne bundle API.
   */
  public function deleteCatsOneBundle($id, $bundle) {
    // For this path its better to use switch case separate in another function.
    $path = $this->catsOneApiPath . $bundle . 's/' . $id;
    if($bundle && $bundle == 'company') {
      $path = $this->catsOneApiPath . 'companies/' . $id;
    }
    $options = $this->catsOneDelete();
    try {
      \Drupal::httpClient()->delete($path, $options);
    }
    catch (RequestException $e) {
    }
  }

  /**
   * Build additional form with custom fields.
   */
  public function buildAdditionalFieldForm($bundle) {
    $fields = [];
    $form = [];
    try {
      $data = $this->getAdditionalFields($bundle);
      if (isset($data)) {
        foreach ($data as $key => $field) {
          $required = 0;
          $field_id = $field->id;
          switch ($field->field->type) {
            case 'file':
              $validators = [
                'file_validate_extensions' => ['docx doc pdf txt odf odt'],
              ];
              $form['field_' . $field_id] = [
                '#type' => 'managed_file',
                '#title' => $field->name,
                '#required' => $required,
                '#upload_location' => 'public://',
                '#upload_validators' => $validators,
              ];
              break;

            case 'text':
              $form['field_' . $field_id] = [
                '#type' => 'textfield',
                '#title' => $field->name,
                '#required' => $required,
              ];
              break;

            case 'multiline':
              $form['field_' . $field_id] = [
                '#type' => 'textarea',
                '#title' => $field->name,
                '#required' => $required,
              ];
              break;

            case 'date':
              $form['field_' . $field_id] = [
                '#type' => 'date',
                '#title' => $field->name,
                '#required' => $required,
              ];
              break;

            case 'number':
              $form['field_' . $field_id] = [
                '#type' => 'number',
                '#title' => $field->name,
                '#required' => $required,
              ];
              break;

            case 'checkbox':
              $form['field_' . $field_id] = [
                '#type' => 'checkbox',
                '#title' => $field->name,
                '#required' => $required,
              ];
              break;

            case 'checkboxes':
              $all_options = [];
              $options = $field->_embedded->custom_field[0]->field->selections;
              foreach ($options as $key => $value) {
                $all_options[$value->id] = $value->label;
              }
              if (in_array($field->linked_custom_field_id, $fields)) {
                $form['field_' . $field_id] = [
                  '#type' => 'checkboxes',
                  '#options' => $all_options,
                  '#title' => $field->name,
                  '#required' => $required,
                ];
              }
              else {
                $form['field_' . $field_id] = [
                  '#type' => 'select',
                  '#options' => $all_options,
                  '#title' => $field->name,
                  '#required' => $required,
                  '#multiple' => TRUE,
                ];
              }
              break;

            case 'select':
              $all_options = [];
              $options = $field->_embedded->custom_field[0]->field->selections;
              foreach ($options as $key => $value) {
                $all_options[$value->id] = $value->label;
              }
              if (in_array($field->linked_custom_field_id, $fields)) {
                $form['field_' . $field_id] = [
                  '#type' => 'radios',
                  '#options' => $all_options,
                  '#title' => $field->name,
                  '#required' => $required,
                ];
              }
              else {
                $form['field_' . $field_id] = [
                  '#type' => 'select',
                  '#title' => $field->name,
                  '#options' => $all_options,
                  '#required' => $required,
                ];
              }
              break;

            case 'radios':
            case 'radio':
              $all_options = [];
              $options = $field->_embedded->custom_field[0]->field->selections;
              foreach ($options as $key => $value) {
                $all_options[$value->id] = $value->label;
              }
              $form['field_' . $field_id] = [
                '#type' => 'radios',
                '#title' => $field->name,
                '#options' => $all_options,
                '#required' => $required,
              ];
              break;
            case 'textarea':
              $form['field_' . $field_id] = [
                '#type' => 'textarea',
                '#title' => $field->name,
                '#required' => $required,
              ];
              break;
            case 'dropdown':
              $list = $data[$key]->field->selections;
              foreach ($list as $k => $v) {
                $options[$v->id] = $v->label;
              }
              $form['field_' . $field_id] = [
                '#type' => 'select',
                '#title' => $field->name,
                '#options' => $options,
                '#required' => $required,
              ];
              break;
          }
        }
      }
      return $form;
    }
    catch (RequestException $e) {
      return FALSE;
    }

  }

  /**
   * Build default catsOne candidate form.
   */
  public function candidateDefaultForm() {
    $form['candidate_fname'] = [
      '#type' => 'textfield',
      '#title' => t('First Name:'),
      '#required' => TRUE,
    ];
    $form['candidate_midname'] = [
      '#type' => 'textfield',
      '#title' => t('Middle Name:'),
    ];
    $form['candidate_lname'] = [
      '#type' => 'textfield',
      '#title' => t('Last Name:'),
      '#required' => TRUE,
    ];
    $form['candidate_mail1'] = [
      '#type' => 'email',
      '#title' => t('Email:'),
      '#required' => TRUE,
    ];
    $form['candidate_mail2'] = [
      '#type' => 'email',
      '#title' => t('Second Email:'),
    ];
    $form['social_media'] = [
      '#type' => 'textfield',
      '#title' => t('Social Media:'),
    ];
    $form['state'] = [
      '#type' => 'textfield',
      '#title' => t('State:'),
    ];
    $form['city'] = [
      '#type' => 'textfield',
      '#title' => t('City:'),
    ];
    $form['street'] = [
      '#type' => 'textfield',
      '#title' => t('Street:'),
    ];
    $form['website'] = [
      '#type' => 'textfield',
      '#title' => t('Website:'),
    ];
    $form['candidate_dob'] = [
      '#type' => 'date',
      '#title' => t('DOB'),
    ];
    $form['source'] = [
      '#type' => 'select',
      '#title' => ('Source'),
      '#options' => [
      ],
    ];
    $form['skills'] = [
      '#type' => 'textarea',
      '#title' => 'Skills',
    ];
    $form['notes'] = [
      '#type' => 'textarea',
      '#title' => 'Notes',
    ];
    $form['work_history'] = [
      '#type' => 'textarea',
      '#title' => 'Work History',
    ];
    $form['upload'] = [
      '#title' => 'Upload',
      '#type' => 'file',
    ];
    $form['hot'] = [
      '#type' => 'checkbox',
      '#title' => t('Hot.'),
    ];
    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => t('Active.'),
    ];
    $form['phone'] = [
      '#type' => 'tel',
      '#title' => 'Phone',
    ];
    $form['owner'] = [
      '#type' => 'textfield',
      '#title' => 'Owner',
    ];
    $form['current_employer'] = [
      '#type' => 'textarea',
      '#title' => 'Current employer',
    ];
    return $form;
  }

  /**
   * Build default catsOne company form.
   */
  public function companyDefaultForm($catsData = []) {
    $form['branch_name'] = [
      '#type' => 'textfield',
      '#title' => t('Branch Name'),
      '#required' => TRUE,
      '#default_value' => ($catsData) ? $catsData->name : NULL,
    ];
    $form['state'] = [
      '#type' => 'textfield',
      '#title' => t('State:'),
      '#default_value' => ($catsData) ? $catsData->address->state : NULL,
    ];
    $form['city'] = [
      '#type' => 'textfield',
      '#title' => t('City:'),
      '#default_value' => ($catsData) ? $catsData->address->city : NULL,
    ];
    $form['street'] = [
      '#type' => 'textfield',
      '#title' => t('Street:'),
      '#default_value' => ($catsData) ? $catsData->address->street : NULL,
    ];
    $form['phone'] = [
      '#type' => 'tel',
      '#title' => 'Phone',
      '#default_value' => ($catsData) ? $catsData->phones->primary : NULL,
    ];
    $form['fax_number'] = [
      '#type' => 'textfield',
      '#title' => t('Fax number'),
      '#default_value' => ($catsData) ? $catsData->phones->fax : NULL,
    ];
    $form['notes'] = [
      '#type' => 'textarea',
      '#title' => 'Notes',
      '#default_value' => ($catsData) ? $catsData->notes : NULL,
    ];
    $form['website'] = [
      '#type' => 'textfield',
      '#title' => t('Website:'),
      '#default_value' => ($catsData) ? $catsData->website : NULL,
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * Get header post options.
   */
  public function catsOnePost($data) {
    $catsApiKey = 'Token ' . \Drupal::state()->get('catsOneApiKey');
    $options = [
      'method' => 'POST',
      'json' => $data,
      'timeout' => 15,
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $catsApiKey,
      ],
    ];
    return $options;
  }
  /**
   * Get header get options.
   */
  public function catsOneGet() {
    $catsApiKey = 'Token ' . \Drupal::state()->get('catsOneApiKey');
    $options = [
      'method' => 'GET',
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $catsApiKey,
      ],
    ];
    return $options;
  }
  /**
   * Get header delete options.
   */
  public function catsOneDelete() {
    $catsApiKey = 'Token ' . \Drupal::state()->get('catsOneApiKey');
    $options = [
      'method' => 'DELETE',
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $catsApiKey,
      ],
    ];
    return$options;
  }

  public function getCompanyById($id) {
    $options = $this->catsOneGet();
    $path = $this->catsOneApiPath . 'companies/' .$id;
    $response = \Drupal::httpClient()->get($path, $options);
    $data = (string) $response->getBody();
    return $data;
  }

  /**
   * Build default catsOne candidate form.
   */
  public function jobDefaultForm() {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#required' => TRUE,
    ];
    $form['state'] = [
      '#type' => 'textfield',
      '#title' => t('State'),
      '#required' => TRUE,
    ];
    $form['city'] = [
      '#type' => 'textfield',
      '#title' => t('City'),
      '#required' => TRUE,
    ];
    $form['category'] = [
      '#type' => 'select',
      '#title' => ('Category'),
      '#options' => $this->catsOneJobCategory()
    ];
    $form['company'] = [
      '#type' => 'select',
      '#title' => ('Company'),
      '#options' => $this->catsOneCompaniesOption(),
    ];
    $form['start_date'] = array(
      '#type' => 'date',
      '#title' => 'Start Date',
      '#required' => TRUE,
    );
    $form['notes'] = [
      '#type' => 'textarea',
      '#title' => 'Notes',
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => 'Description',
    ];
    $form['hot'] = [
      '#type' => 'checkbox',
      '#title' => t('Hot'),
    ];
    $form['publish'] = [
      '#type' => 'checkbox',
      '#title' => t('Publish'),
    ];
    return $form;
  }

  public function catsOneJobCategory() {
    $options = [
      'Accounting/Finance' => 'Accounting/Finance',
      'Administrative' => 'Administrative',
      'Architecture/Engineering' => 'Architecture/Engineering',
      'Art/Media/Design' => 'Art/Media/Design',
      'Banking/Loans' => 'Banking/Loans',
      'Biotech/Pharmaceutical' => 'Biotech/Pharmaceutical',
      'Computer/Software' => 'Computer/Software',
      'Construction/Facilities' => 'Construction/Facilities',
      'Customer Service' => 'Customer Service',
      'Education' => 'Education',
      'General Labor' => 'General Labor',
      'Government/Military' => 'Government/Military',
      'Healthcare' => 'Healthcare',
      'Hospitality/Travel' => 'Hospitality/Travel',
      'Human Resources' => 'Human Resources',
      'Information Technology' => 'Information Technology',
      'Law Enforcement/Security' => 'Law Enforcement/Security',
      'Legal' => 'Legal',
      'Manufacturing' => 'Manufacturing',
      'Marketing/Public Relations' => 'Marketing/Public Relations',
      'Real Estate' => 'Real Estate',
      'Restaurant/Food Service' => 'Restaurant/Food Service',
      'Retail' => 'Retail',
      'Sales' => 'Sales',
      'Science/Research' => 'Science/Research',
      'Telecommunications' => 'Telecommunications',
      'Transportation/Logistics' => 'Transportation/Logistics',
      'Volunteering/Non-Profit' => 'Volunteering/Non-Profit',
      'Writing/Editing' => 'Writing/Editing',
    ];
    return $options;
  }

  public function getCatsOneCompanies() {
    $data = [];
    $path = $this->catsOneApiPath . 'companies';
    $options = $this->catsOneGet();

    try {
      $response = \Drupal::httpClient()->get($path,$options);
      $data = (string) $response->getBody();
    }
    catch (RequestException $e) {
      drupal_set_message('Something went wrong ! Please try again.', 'warning');
    }

    return $data;
  }

  public function catsOneCompaniesOption() {
    $cats_companies = [];
    $company = $this->getCatsOneCompanies();
    $companies_decoded = json_decode($company);
    $companies = ($companies_decoded->_embedded->companies) ? $companies_decoded->_embedded->companies : NULL;
    foreach ($companies as $key => $value) {
      $cats_companies[$value->id] = $value->name;
    }
    return $cats_companies;
  }
}
