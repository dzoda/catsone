<?php

namespace Drupal\catsOne\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\catsOne\Services\CatsOneService;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class CatsOne.
 *
 * @package Drupal\catsOne\Form
 */
class CatsOneCandidate extends FormBase {

  private $catsOneService;

  public function __construct(ConfigFactoryInterface $config_factory, CatsOneService $catsOneService) {
    $this->setConfigFactory($config_factory);
    $this->catsOneService = $catsOneService;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory'),
      $container->get('catsone.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cats_one_candidate';
  }

  /**
   * {@inheritdoc}
   *
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $addCandidate = Url::fromRoute('catsOne.add_candidate');
    $addCandidate->setOptions([
      'attributes' => [
        'class' => ['use-ajax', 'button'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 1200]),
      ],
    ]);
    $addCandidate = Link::fromTextAndUrl(t('Add new'), $addCandidate)->toString();
    $bundle = 'candidate';
    $header = array(
      array('data' => $this->t('Candidate ID')),
      array('data' => $this->t('First Name')),
      array('data' => $this->t('Last Name')),
      array('data' => $this->t('Email')),
      array('data' => $this->t('Bundle')),
      array('data' => $this->t('Operations')),
      array('data' => $addCandidate),
    );
    $result = $this->catsOneService->loadCatsOneBundleTable($bundle, $header);
    // Populate the rows.
    $rows = array();
    foreach($result as $row) {
      $link_url_delete = Url::fromRoute('catsOne.delete_candidate_modal', ['candidate' => $row->id, 'js' =>'nojs']);
      $link_url_delete->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 800]),
        ],
      ]);
      $delete = Link::fromTextAndUrl(t('Delete'), $link_url_delete)->toString();
      $link_url_edit = Url::fromRoute('catsOne.edit_candidate', ['candidate' => $row->id, 'js' =>'nojs']);
      $link_url_edit->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 800]),
        ],
      ]);
      $edit = Link::fromTextAndUrl(t('Edit'), $link_url_edit)->toString();
      $decoded_data = json_decode($row->cats_one_data);
      $rows[] = [
        'data' => [
          'candidate_id' => $decoded_data->id,
          'candidate_fname' => $decoded_data->first_name,
          'candidate_lname' => $decoded_data->last_name,
          'candidate_email' => $decoded_data->emails->primary,
          'bundle' => $bundle,
          $edit,
          $delete,
        ],
      ];
    }
    // Generate the table.
    $form['config_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
    // Finally add the pager.
    $form['pager'] = array(
      '#type' => 'pager'
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}
