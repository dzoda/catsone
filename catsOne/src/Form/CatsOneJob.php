<?php

namespace Drupal\catsOne\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\catsOne\Services\CatsOneService;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class CatsOne.
 *
 * @package Drupal\catsOne\Form
 */
class CatsOneJob extends FormBase {

  private $catsOneService;

  public function __construct(ConfigFactoryInterface $config_factory, CatsOneService $catsOneService) {
    $this->setConfigFactory($config_factory);
    $this->catsOneService = $catsOneService;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory'),
      $container->get('catsone.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cats_one_job';
  }

  /**
   * {@inheritdoc}
   *
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $addCandidate = Url::fromRoute('catsOne.add_job');
    $addCandidate->setOptions([
      'attributes' => [
        'class' => ['use-ajax', 'button'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 1200]),
      ],
    ]);
    $addCandidate = Link::fromTextAndUrl(t('Add new'), $addCandidate)->toString();
    $bundle = 'job';
    $header = array(
      array('data' => $this->t('Job ID')),
      array('data' => $this->t('Job name')),
//      array('data' => $this->t('Company name')),
      array('data' => $this->t('Job start date')),
      array('data' => $this->t('Bundle')),
      array('data' => $this->t('Operations')),
      array('data' => $addCandidate),
    );
    $result = $this->catsOneService->loadCatsOneBundleTable($bundle, $header);
    // Populate the rows.
    $rows = array();
    foreach($result as $row) {
      $link_url_delete = Url::fromRoute('catsOne.delete_candidate_modal', ['candidate' => $row->id, 'js' =>'nojs']);
      $link_url_delete->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 800]),
        ],
      ]);
      $delete = Link::fromTextAndUrl(t('Delete'), $link_url_delete)->toString();
      $link_url_edit = Url::fromRoute('catsOne.edit_candidate', ['candidate' => $row->id, 'js' =>'nojs']);
      $link_url_edit->setOptions([
        'attributes' => [
          'class' => ['use-ajax', 'button'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => 800]),
        ],
      ]);
      $edit = Link::fromTextAndUrl(t('Edit'), $link_url_edit)->toString();
      $decoded_data = json_decode($row->cats_one_data);
      // Pay attention! This is one call per row. CatsOne has limit for requests about 500 per hour.
//      $company = $this->catsOneService->getCompanyById($decoded_data->company_id);
//      $company = json_decode($company);
      $rows[] = [
        'data' => [
          'job_id' => $decoded_data->id,
          'job_name' => $decoded_data->title,
//          'job_company' => $company->name,
          'job_start_date' => $decoded_data->start_date,
          'bundle' => $bundle,
          $edit,
          $delete,
        ],
      ];
    }
    // Generate the table.
    $form['config_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
    // Finally add the pager.
    $form['pager'] = array(
      '#type' => 'pager'
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}
