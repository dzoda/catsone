<?php

namespace Drupal\catsOne\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\catsOne\Services\CatsOneService;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class CatsOne.
 *
 * @package Drupal\catsOne\Form
 */
class CatsOneSettings extends ConfigFormBase {

  private $catsOneService;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cats_one_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cats_one.settings',
    ];
  }

  public function __construct(ConfigFactoryInterface $config_factory, CatsOneService $catsOneService) {
    $this->setConfigFactory($config_factory);
    $this->catsOneService = $catsOneService;

    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory'),
      $container->get('catsone.api_service')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $default_key = ($this->catsOneService->state->get('catsOneApiKey')) ? $this->catsOneService->state->get('catsOneApiKey') : '';

    $form['cats_one_header'] = [
      '#type' => 'markup',
      '#markup' => 'CatsOne API path',
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',

    ];

    $form['cats_one_path'] = [
      '#type' => 'markup',
      '#markup' => 'https://api.catsone.com/v3/site',
    ];

    $form['cats_one_api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Enter catsOne API key'),
      '#default_value' => $default_key,
      '#description' => $this->t('This is CatsOne API key validation.'),
    ];

    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $catsOneKey = (!empty($form['cats_one_api_key']['#value'])) ? $form['cats_one_api_key']['#value'] : '';
    $this->catsOneService->getCatsOneInfo($catsOneKey);
  }
}
