<?php

namespace Drupal\catsOne\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\catsOne\Services\CatsOneService;
use Drupal\Core\Link;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class CatsOne.
 *
 * @package Drupal\catsOne\Form
 */
class CatsOneEditCompany extends FormBase {

  private $catsOneService;


  public function __construct(ConfigFactoryInterface $config_factory, CatsOneService $catsOneService) {
    $this->setConfigFactory($config_factory);
    $this->catsOneService = $catsOneService;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory'),
      $container->get('catsone.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cats_one_edit_company';
  }

  /**
   * {@inheritdoc}
   *
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $current_path = \Drupal::request()->getpathInfo();
    $arg = explode('/', $current_path);
    $bundle_id = ($arg[6] && is_numeric($arg[6])) ? (int) $arg[6] : NULL;
    $cats_company = $this->catsOneService->loadCatsOneBundle([$bundle_id]);
    $cats_company_data = $cats_company[$bundle_id];
    $cats_company_data = $cats_company_data->get('cats_one_data')->first()->getValue();
    $cats_company_data = json_decode($cats_company_data['value']);

    $form = $this->catsOneService->companyDefaultForm($cats_company_data);



    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValues();
    $data = [
      'first_name' => $value['candidate_fname'],
      'middle_name' => $value['candidate_midname'],
      'last_name' => $value['candidate_lname'],
      'emails' => [
        'primary' => $value['candidate_mail1'],
      ],
      'phones' => [
        'cell' => '',
      ],
      'custom_fields' => [],
    ];

    /* -- Custom Fields Part -- */
    foreach ($value as $k => $v) {
      if (strpos($k, 'field') !== false) {
        $custom_field_id = explode('_', $k);
        if ($v) {
          $custom_field_data = new \stdClass();
          $custom_field_data->id = (int) $custom_field_id[1];
          $custom_field_data->value = $v;
          $data['custom_fields'][] = $custom_field_data;
        }
      }
    }
    // Path for adding candidate.
    $path = $this->catsOneService->catsOneApiPath . 'candidates?check_duplicate=true';
    // Options for creating request.
    $options = $this->catsOneService->catsOnePost($data);
    try {
     $response = \Drupal::httpClient()->post($path, $options);
      $status = $response->getStatusCode();
    if($status == 201) {
      $candidate_info = $response->getHeaders();
      $opt = $this->catsOneService->catsOneGet();
      $new_path = $candidate_info['Location'][0];
      $get_candidate = \Drupal::httpClient()->get($new_path, $opt);
      $new_data = (string) $get_candidate->getBody();
      $this->catsOneService->importCatsOneBundle('candidate', $new_data);
    }
    }
    catch (RequestException $e) {
    }
    drupal_set_message('Candidate is successefuly created', 'status');
    $url = Url::fromUri("internal:/admin/structure/cats-one/candidate");
    $form_state->setRedirectUrl($url);
  }
}
