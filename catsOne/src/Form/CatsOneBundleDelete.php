<?php

namespace Drupal\catsOne\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\catsOne\Services\CatsOneService;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class CatsOne.
 *
 * @package Drupal\catsOne\Form
 */
class CatsOneBundleDelete extends FormBase {

  private $catsOneService;


  public function __construct(ConfigFactoryInterface $config_factory, CatsOneService $catsOneService) {
    $this->setConfigFactory($config_factory);
    $this->catsOneService = $catsOneService;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory'),
      $container->get('catsone.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cats_one_bundle_delete';
  }

  /**
   * {@inheritdoc}
   *
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_path = \Drupal::request()->getpathInfo();
    $arg = explode('/', $current_path);
    // Add drupal way for handling this.
    $bundle_id = ($arg[6] && is_numeric($arg[6])) ? $arg[6] : NULL ;
    $bundle = $this->catsOneService->loadCatsOneCandidateById($bundle_id);
    $bundle_type = $bundle->get('cats_one_type')->first()->getValue();
    $bundle_type = $bundle_type['value'];
    $bundle_data = $bundle->get('cats_one_data')->first()->getValue();
    $bundle_data = json_decode($bundle_data['value']);

    // Think about better way for this logic :)
    if (!empty($bundle_type) && $bundle_type == 'candidate') {
      $info = [
        'candidate_fname' => $bundle_data->first_name,
        'candidate_lname' => $bundle_data->last_name,
        'candidate_email' => $bundle_data->emails->primary,
      ];
    }
    elseif (!empty($bundle_type) && $bundle_type == 'company'){
      $info = [
        'company_title' => $bundle_data->name,
      ];
    }
    else {
      $info = [
        'job_title' => $bundle_data->title,
      ];
    }
    $bundle_cats_id = $bundle_data->id;

    $form['cats_one_header'] = [
      '#type' => 'markup',
      '#markup' => 'Delete CatsOne ' . $bundle_type,
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    $form['entity_id'] = ['#type' => 'hidden', '#value' => $bundle_id];
    $form['bundle_cats_one_id'] = ['#type' => 'hidden', '#value' => $bundle_cats_id];
    $form['bundle_type'] = ['#type' => 'hidden', '#value' => $bundle_type];
    $form['candidate_msg'] = [
      '#type' => 'markup',
      '#markup' => 'Are you sure you want to delete this ' . $bundle_type,
      '#weight' => 2,
      '#suffix' => '</br>',
    ];

    foreach ($info as $k => $v) {
      $form[$k] = [
        '#type' => 'markup',
        '#markup' => $v,
        '#weight' => 2,
        '#suffix' => '</br>',
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Delete',
      '#weight' => 10,
      '#prefix' => '</br>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $bundle_id = $form['entity_id']['#value'];
    $bundle_cats_one_id = $form['bundle_cats_one_id']['#value'];
    $bundle_type = $form['bundle_type']['#value'];
    $this->catsOneService->deleteCatsOneBundle($bundle_cats_one_id, $bundle_type);
    $this->catsOneService->deleteCatsOneBundleById($bundle_id);

    drupal_set_message($bundle_type . ' is successefuly deleted', 'status');
    $url = Url::fromUri("internal:/admin/structure/cats-one/" . $bundle_type);
    $form_state->setRedirectUrl($url);

  }
}
