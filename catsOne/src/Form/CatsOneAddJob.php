<?php

namespace Drupal\catsOne\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\catsOne\Services\CatsOneService;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class CatsOne.
 *
 * @package Drupal\catsOne\Form
 */
class CatsOneAddJob extends FormBase {

  private $catsOneService;


  public function __construct(ConfigFactoryInterface $config_factory, CatsOneService $catsOneService) {
    $this->setConfigFactory($config_factory);
    $this->catsOneService = $catsOneService;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory'),
      $container->get('catsone.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cats_one_add_candidate';
  }

  /**
   * {@inheritdoc}
   *
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = $this->catsOneService->jobDefaultForm();
    $additional_fields = $this->catsOneService->buildAdditionalFieldForm('jobs');
    foreach ($additional_fields as $form_id => $form_value) {
      $form[$form_id] = $form_value;
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValues();
    $data = array (
      'title' => $value['title'],
      'location' =>
        array (
          'city' => $value['city'],
          'state' => $value['state'],
          'postal_code' => '',
        ),
      'country_code' => NULL,
      'company_id' => (int) $value['company'],
      'department_id' => NULL,  // This is hardcoded for now.
      'recruiter_id' => 1111,  // This is hardcoded for now.
      'category_name' => $value['category'],
      'is_hot' => $value['hot'],
      'start_date' => $value['start_date'],
      'salary' => '',
      'max_rate' => '',
      'duration' => '',
      'type' => '',
      'openings' => 0,
      'external_id' => '',
      'description' => $value['description'],
      'notes' => $value['notes'],
      'contact_id' => 1111,
      'workflow_id' => '',
      'publish' => $value['publish'],
      'custom_fields' => [],
    );
    /* -- Custom Fields Part -- */
    foreach ($value as $k => $v) {
      if (strpos($k, 'field') !== false) {
        $custom_field_id = explode('_', $k);
        if ($v) {
          $custom_field_data = new \stdClass();
          $custom_field_data->id = (int) $custom_field_id[1];
          $custom_field_data->value = $v;
          $data['custom_fields'][] = $custom_field_data;
        }
      }
    }
    // Path for adding candidate.
    $path = $this->catsOneService->catsOneApiPath . 'jobs?check_duplicate=true';
    // Options for creating request.
    $options = $this->catsOneService->catsOnePost($data);
    try {
     $response = \Drupal::httpClient()->post($path, $options);
      $status = $response->getStatusCode();
    if($status == 201) {
      $job_info = $response->getHeaders();
      $opt = $this->catsOneService->catsOneGet();
      $new_path = $job_info['Location'][0];
      $new_response = \Drupal::httpClient()->get($new_path, $opt);
      $new_data = (string) $new_response->getBody();
      $this->catsOneService->importCatsOneBundle('job', $new_data);
    }
    }
    catch (RequestException $e) {
    }
    drupal_set_message('Job is successefuly created', 'status');
    $url = Url::fromUri("internal:/admin/structure/cats-one/job");
    $form_state->setRedirectUrl($url);
  }
}
