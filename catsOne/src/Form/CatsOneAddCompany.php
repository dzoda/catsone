<?php

namespace Drupal\catsOne\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\catsOne\Services\CatsOneService;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class CatsOne.
 *
 * @package Drupal\catsOne\Form
 */
class CatsOneAddCompany extends FormBase {

  private $catsOneService;


  public function __construct(ConfigFactoryInterface $config_factory, CatsOneService $catsOneService) {
    $this->setConfigFactory($config_factory);
    $this->catsOneService = $catsOneService;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory'),
      $container->get('catsone.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cats_one_add_candidate';
  }

  /**
   * {@inheritdoc}
   *
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = $this->catsOneService->companyDefaultForm();
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $value = $form_state->getValues();
    $data = [
      'name' => $value['branch_name'],
      'owner_id' => 1111,
      'website' => $value['website'],
      'address' => [
          'street' => $value['street'],
          'city' => $value['city'],
          'state' => $value['street'],
          'postal_code' => NULL,
        ],
      'country_code' => NULL,
      'phones' => [
          'primary' => $value['phone'],
          'fax' => $value['fax_number'],
        ],
      'entered_by_id' => NULL,
      'social_media_urls' => [
        ],
      'notes' => $value['notes'],
      'is_hot' => false,
      'key_technologies' => '',
      'billing_contact_id' => NULL,
      'custom_fields' => [],
    ];
    // Path for adding candidate.
    $path = $this->catsOneService->catsOneApiPath . 'companies?check_duplicate=true';
    // Options for creating request.
    $options = $this->catsOneService->catsOnePost($data);
    try {
     $response = \Drupal::httpClient()->post($path, $options);
      $status = $response->getStatusCode();
    if($status == 201) {
      $candidate_info = $response->getHeaders();
      $opt = $this->catsOneService->catsOneGet();
      $new_path = $candidate_info['Location'][0];
      $new_response = \Drupal::httpClient()->get($new_path, $opt);
      $new_data = (string) $new_response->getBody();
      $this->catsOneService->importCatsOneBundle('company', $new_data);
    }
      drupal_set_message('Company is successefuly created', 'status');
      $url = Url::fromUri("internal:/admin/structure/cats-one/company");
      $form_state->setRedirectUrl($url);
    }
    catch (RequestException $e) {
      drupal_set_message('Company is not created', 'warning');
    }
    $url = Url::fromUri("internal:/admin/structure/cats-one/company");
    $form_state->setRedirectUrl($url);
  }
}
