<?php

namespace Drupal\catsOne\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\catsOne\Services\CatsOneService;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class CatsOne.
 *
 * @package Drupal\catsOne\Form
 */
class CatsOneBundle extends FormBase {

  private $catsOneService;


  public function __construct(ConfigFactoryInterface $config_factory, CatsOneService $catsOneService) {
    $this->setConfigFactory($config_factory);
    $this->catsOneService = $catsOneService;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('config.factory'),
      $container->get('catsone.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cats_one_bundle';
  }

  /**
   * {@inheritdoc}
   *
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['cats_one_header'] = [
      '#type' => 'markup',
      '#markup' => 'CatsOne Bundles',
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',

    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Import',
      '#description' => 'Import all Bundles from CatsOne.',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $bundle_c = 'candidate';
    $data = $this->catsOneService->getAllCatsOneCandidates();
    $data_decoded =  json_decode($data);
    $candidates = $data_decoded->_embedded->candidates;

    // Import all candidates from CatsOne.
    foreach ($candidates as $k => $v) {
      $this->catsOneService->importCatsOneBundle($bundle_c, json_encode($v));
    }
    // Import all jobs from CatsOne.
    $bundle_j = 'job';
    $job_data = $this->catsOneService->getAllCatsOneJobs();
    $job_data_decoded = json_decode($job_data);
    $jobs = $job_data_decoded->_embedded->jobs;

    foreach ($jobs as $key => $value) {
      $this->catsOneService->importCatsOneBundle($bundle_j, json_encode($value));
    }
    // Import all companies from CatsOne.
    $bundle_c = 'company';
    $data = $this->catsOneService->getAllCatsOneCompanies();
    $data_decoded =  json_decode($data);
    $candidates = $data_decoded->_embedded->companies;

    foreach ($candidates as $k => $v) {
      $this->catsOneService->importCatsOneBundle($bundle_c, json_encode($v));
    }
  }
}
