<?php

namespace Drupal\catsOne\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cats one entity entities.
 *
 * @ingroup catsOne
 */
class CatsOneEntityDeleteForm extends ContentEntityDeleteForm {


}
