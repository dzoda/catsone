<?php

namespace Drupal\catsOne;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Cats one entity entities.
 *
 * @ingroup catsOne
 */
class CatsOneEntityListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Cats one entity ID');
    $header['cats_one_data'] = $this->t('Data');
    $header['cats_one_type'] = $this->t('CatsOne Bundle');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\catsOne\Entity\CatsOneEntity */
    $row['id'] = $entity->id();
    $row['cats_one_data'] = substr($entity->getData(),0,100).'...';
    $row['cats_one_type'] = $entity->getType();

    return $row + parent::buildRow($entity);
  }

}
