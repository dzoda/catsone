<?php

namespace Drupal\catsOne;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cats one entity entity.
 *
 * @see \Drupal\catsOne\Entity\CatsOneEntity.
 */
class CatsOneEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\catsOne\Entity\CatsOneEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cats one entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published cats one entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit cats one entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete cats one entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cats one entity entities');
  }

}
