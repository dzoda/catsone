<?php

namespace Drupal\catsOne\Controller;

use Drupal\catsOne\Services\CatsOneService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBuilder;



class CatsOneController extends ControllerBase {


  private $catsOneService;
  protected  $formBuilder;

  public function __construct(CatsOneService $catsOneService, FormBuilder $formBuilder) {
    $this->catsOneService = $catsOneService;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('catsone.api_service'),
      $container->get('form_builder')
    );
  }



}

