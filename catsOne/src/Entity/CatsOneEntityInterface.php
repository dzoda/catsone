<?php

namespace Drupal\catsOne\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cats one entity entities.
 *
 * @ingroup catsOne
 */
interface CatsOneEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Cats one entity name.
   *
   * @return string
   *   Name of the Cats one entity.
   */
  public function getName();

  /**
   * Sets the Cats one entity name.
   *
   * @param string $name
   *   The Cats one entity name.
   *
   * @return \Drupal\catsOne\Entity\CatsOneEntityInterface
   *   The called Cats one entity entity.
   */
  public function setName($name);

  /**
   * Gets the Cats one entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cats one entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Cats one entity creation timestamp.
   *
   * @param int $timestamp
   *   The Cats one entity creation timestamp.
   *
   * @return \Drupal\catsOne\Entity\CatsOneEntityInterface
   *   The called Cats one entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Cats one entity published status indicator.
   *
   * Unpublished Cats one entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Cats one entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Cats one entity.
   *
   * @param bool $published
   *   TRUE to set this Cats one entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\catsOne\Entity\CatsOneEntityInterface
   *   The called Cats one entity entity.
   */
  public function setPublished($published);

}
