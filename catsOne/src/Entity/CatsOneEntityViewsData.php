<?php

namespace Drupal\catsOne\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Cats one entity entities.
 */
class CatsOneEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
