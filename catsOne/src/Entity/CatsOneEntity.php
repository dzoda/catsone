<?php

namespace Drupal\catsOne\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Cats one entity entity.
 *
 * @ingroup catsOne
 *
 * @ContentEntityType(
 *   id = "cats_one_entity",
 *   label = @Translation("Cats one entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\catsOne\CatsOneEntityListBuilder",
 *     "views_data" = "Drupal\catsOne\Entity\CatsOneEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\catsOne\Form\CatsOneEntityForm",
 *       "add" = "Drupal\catsOne\Form\CatsOneEntityForm",
 *       "edit" = "Drupal\catsOne\Form\CatsOneEntityForm",
 *       "delete" = "Drupal\catsOne\Form\CatsOneEntityDeleteForm",
 *     },
 *     "access" = "Drupal\catsOne\CatsOneEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\catsOne\CatsOneEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cats_one_entity",
 *   admin_permission = "administer cats one entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "cats_one_data" = "cats_one_data",
 *     "cats_one_type" = "cats_one_type",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cats_one_entity/{cats_one_entity}",
 *     "add-form" = "/admin/structure/cats_one_entity/add",
 *     "edit-form" = "/admin/structure/cats_one_entity/{cats_one_entity}/edit",
 *     "delete-form" = "/admin/structure/cats_one_entity/{cats_one_entity}/delete",
 *     "collection" = "/admin/structure/cats_one_entity",
 *   },
 *   field_ui_base_route = "cats_one_entity.settings"
 * )
 */
class CatsOneEntity extends ContentEntityBase implements CatsOneEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->get('cats_one_data')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->get('cats_one_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['cats_one_data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Data'))
      ->setDescription(t('The data of the cats Candidate Status entity.'))
      ->setSettings(array(
        'default_value' => '',
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -6,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textarea',
        'weight' => -6,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['cats_one_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cats One bundle'))
      ->setDescription(t('The name of the Cats one bundle.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Cats one entity is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
