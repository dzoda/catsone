<?php

/**
 * @file
 * Contains cats_one_entity.page.inc.
 *
 * Page callback for Cats one entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Cats one entity templates.
 *
 * Default template: cats_one_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cats_one_entity(array &$variables) {
  // Fetch CatsOneEntity Entity Object.
  $cats_one_entity = $variables['elements']['#cats_one_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
